# Otoklik Test

## Requirements

- [Node >=16.13.2](https://nodejs.org)

### Setup
---
Clone the repo and follow below steps.
1. Run `npm install`
2. Copy `.env.example` to `.env` Example for linux users : `cp .env.example .env`
3. Run `npm run dev` to Start the application.
