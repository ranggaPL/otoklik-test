import { Nav } from 'react-bootstrap';

import Layout from '../components/layout'
import Post from '../components/post'

export async function getServerSideProps() {
  const res = await fetch(`${process.env.API_URL}/posts`)
  const data = await res.json()

  return { props: { posts: data } }
}

export default function Index({ posts }) {
  const listPosts = posts.map((value) =>
    <Post key={value.id} id={value.id} title={value.title} content={value.content} published_at={value.published_at} isContentTruncate={true}></Post>
  );

  return (
    <Layout
      title='Otoklik Test | Show all Posts'
      description='Otoklik Test | Show all Posts'
    >
      <Nav className="justify-content-end">
        <Nav.Item>
          <Nav.Link href="/create">Create a Post</Nav.Link>
        </Nav.Item>
      </Nav>

      {posts.length > 0
        ? listPosts
        : <p className='text-center'>No Data</p>
      }
    </Layout>
  )
}