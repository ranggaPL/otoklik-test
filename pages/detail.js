import { useState } from 'react';
import { useRouter } from 'next/router'
import { Nav } from 'react-bootstrap';

import Layout from '../components/layout'
import Post from '../components/post'
import DeleteConfirmation from '../components/delete-confirmation'

export async function getServerSideProps({ query }) {
  try {
    const res = await fetch(`${process.env.API_URL}/posts/${query.id}`)
    const data = await res.json()
    return { props: { post: data } }
  } catch(error) {
    return { props: { post: null } }
  }
}

export default function Detail({ post }) {
  const router = useRouter()
  const [deleting, setDeleting] = useState(false);
  const [deleteConfirmation, setDeleteConfirmation] = useState(false);

  const onDeleteConfirmation = () => setDeleteConfirmation(true)
  const onCancelDelete = () => setDeleteConfirmation(false)

  const onDelete = async () => {
    try {
      setDeleting(true)
      const res = await fetch(`${process.env.API_URL}/posts/${post.id}`, {
        method: 'DELETE',
      })
      const data = await res.json()
      router.push(`/`)
    } catch (err) {
      setDeleting(false)
      console.log(err)
    }
  }

  return (
    <Layout
      title='Otoklik Test | Detail Post'
      description='Otoklik Test | Detail Post'
    >
      {post != null
        ? <>
            <Nav className="justify-content-end">
              <Nav.Item>
                <Nav.Link href={"/edit?id=" + post.id}>Edit</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link onClick={onDeleteConfirmation}>Delete</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link onClick={() => router.back()}>Back</Nav.Link>
              </Nav.Item>
            </Nav>

            <Post id={post.id} title={post.title} content={post.content} published_at={post.published_at} isContentTruncate={false}></Post>
          </>
        : <p className='text-center'>Data not found</p>
      }

      <DeleteConfirmation show={deleteConfirmation} onDelete={onDelete} onCancel={onCancelDelete} deleting={deleting} />
      
    </Layout>
  )
}