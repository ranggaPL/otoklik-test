import { useState } from 'react';
import { useRouter } from 'next/router'
import { Button, Form, Nav } from 'react-bootstrap';

import Layout from '../components/layout'
import FormPost from '../components/form-post'

export default function Create() {
  const router = useRouter()
  const [submitting, setSubmitting] = useState(false);

  const onSubmit = async (data) => {
    const params = data
    try {
      setSubmitting(true)
      const res = await fetch(`${process.env.API_URL}/posts`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(params),
      })
      const data = await res.json()
      router.push(`/detail?id=${data.id}`)
    } catch (err) {
      setSubmitting(false)
      console.log(err)
    }
  }

  return (
    <Layout
      title='Otoklik Test | Create new Post'
      description='Otoklik Test | Create new Post'
    >
      <Nav className="justify-content-end">
        <Nav.Item>
          <Nav.Link onClick={() => router.back()}>Back</Nav.Link>
        </Nav.Item>
      </Nav>
      
      <FormPost onSubmit={onSubmit} submitting={submitting}/>
    </Layout>
  )
}