import { parseISO, format } from 'date-fns'

import styles from './post.module.css'

export default function Post(props) {
  const publishedAt = parseISO(props.published_at)

  return (
    <article>
      <h3 className={styles.title}><a href={ "/detail?id=" + props.id}>{props.title}</a></h3>
      <p className={props.isContentTruncate ? 'text-truncate': ''}>
        {props.content || 'No Content'}
      </p>
      <time className={styles.published_at}>{format(publishedAt, 'EEE LLLL d, yyyy')}</time>
    </article>
  )
}