import { Button, Modal } from 'react-bootstrap';

export default function DeleteConfirmation({show, onDelete, onCancel, deleting}) {
  return (
    <Modal show={show} onHide={onCancel} animation={false} backdrop="static">
      <Modal.Header>
        <Modal.Title>Delete Confirmation</Modal.Title>
      </Modal.Header>
      <Modal.Body>Are you sure you want to delete this item?</Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={onCancel} disabled={deleting}>
          Cancel
        </Button>
        <Button variant="danger" onClick={onDelete} disabled={deleting}>
          Sure
        </Button>
      </Modal.Footer>
    </Modal>
  )
}