import { useState } from 'react';
import { Button, Form, Nav } from 'react-bootstrap';

export default function FormPost({ post, onSubmit, submitting }) {
  const [validated, setValidated] = useState(false);
  const [data, setData] = useState({title: post?.title || '', content: post?.content || ''});
  

  const handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    setData({
      ...data,
      [name]: value
    });
  }

  const handleSubmit = (event) => {
    const form = event.currentTarget;
    event.preventDefault();
    event.stopPropagation();

    setValidated(true);
    if (form.checkValidity() === true) {
      onSubmit(data)
    }
  };
  
  return (
    <Form noValidate validated={validated} onSubmit={handleSubmit}>
      <Form.Group className="mb-3" controlId="formTitle">
        <Form.Label>Title</Form.Label>
        <Form.Control
          required
          name="title"
          type="text"
          placeholder="Enter title"
          value={data.title}
          onChange={handleInputChange} />
      </Form.Group>

      <Form.Group className="mb-3" controlId="formContent">
        <Form.Label>Content</Form.Label>
        <Form.Control
          required
          name="content"
          as="textarea"
          placeholder="Enter content"
          style={{ height: '100px' }}
          value={data.content}
          onChange={handleInputChange} />
      </Form.Group>

      <Button variant="primary" type="submit" disabled={submitting}>
        Save
      </Button>
    </Form>
  )
}