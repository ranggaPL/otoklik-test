import Head from 'next/head'
import { Container } from 'react-bootstrap';

import styles from './layout.module.css'

export default function Layout(props) {
  return (
    <>
      <Head>
        <title>{props.title || "Otoklik Test"}</title>
        <meta name="description" content={props.description || "Otoklik Test"} />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Container className={styles.container}>
        <h1 className={styles.title}>Welcome to <a href='/'>Otoklik Test</a></h1>
        {props.children}
      </Container>

      <footer className={styles.footer}>Copyright 2022</footer>
    </>
  )
}